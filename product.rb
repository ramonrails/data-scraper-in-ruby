#  * product is a DOM object as parsed through Nokogiri
class Product
  attr_accessor :node, :url, :uri, :identifier
  attr_accessor :item_name, :brand_name, :link, :image, :regular_price, :sale_price

  def initialize( arg, options = {})
    #  keep XML node DOM object
    if [ 'Nokogiri::XML::Element', 'Nokogiri::XML::Node'].include?( arg.class.name)
      self.node = arg
    end
    #  * remember optional parameters
    [ :business, :url, :identifier].each do |_option|
      if (options.class.name == 'Hash') && options.has_key?( _option)
        self.send( "#{_option}=", options[ _option])
      end
    end
    self.uri = URI::parse( url) # keep URL parsed
    parse_attributes # parse attributes from HTML
    swap_price_if_required # swap the price if needed
  end

  # checks if the sale_price was automatically lowered than regular_price
  def sale_price_lower?
    sale_price_value < regular_price_value
  end
  # ===================
  # = private methods =
  # ===================
  private

  def parse_attributes
    self.link          = uri.merge( node.at( identifier[ :link])['href']).to_s
    self.image         = uri.merge( node.at( identifier[ :image])['src']).to_s
    self.item_name     = node.at( identifier[ :item_name]     ).inner_text
    self.brand_name    = node.at( identifier[ :brand_name]    ).inner_text
    self.regular_price = node.at( identifier[ :regular_price] ).inner_text.gsub(/[^\$0-9\.]/,"") rescue ''
    self.sale_price    = node.at( identifier[ :sale_price]    ).inner_text.gsub(/[^\$0-9\.]/,"") rescue ''
  end

  def swap_price_if_required
    (self.regular_price, self.sale_price = sale_price, regular_price) unless sale_price_lower?
    sale_price_lower?
  end

  def regular_price_value
    regular_price.gsub(/[^0-9]/,"").to_i
  end

  def sale_price_value
    sale_price.gsub(/[^0-9]/,"").to_i
  end
end
