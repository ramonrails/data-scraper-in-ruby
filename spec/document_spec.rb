require "./spec/spec_helper"

describe Document do
  #  * pick the name of a file and instantiate a document
  before do
    identifier = {
      :product       => '.producttile',
      :id            => 'id',
      :item_name     => 'span.displayname',
      :brand_name    => 'span.designername',
      :image         => '.productimage a img',
      :link          => '.productimage a',
      :regular_price => '.standardprice',
      :sale_price    => '.salesprice'
    }
    @args     = { :business => '<BusinessName>', :url => 'http://www.<businessname>.com/<path_to_page>.html', :identifier => identifier }
    @name     = '<some_name>.html'
    @document = Document.new( @name, @args)
  end

  #  * some methods on document class
  context "have methods to read and parse HTML file" do
    [ :find_file, :file_name, :exists?, :data, :dom_object, :business, :url, :identifier, :products].each do |action|
      specify { @document.should respond_to( action)} # read method for document object
    end
  end

  #  * ability to read an HTML file
  #  * extension ".html" is optional
  context "should accept a file with extension" do
    specify { @document.should_not be_blank }
    specify { @document.file_name.should_not be_blank }
    specify { @document.file_name.should include( @name) }
    specify { @document.exists?.should be_true }
  end

  #  * assuming the HTML file has valid markup
  context "should read data from file" do
    specify { @document.data.should_not be_blank }
    specify { @document.dom_object.should_not be_blank }
  end

  context "should take optional parameters" do
    specify { @document.url.should_not be_blank }
    specify { @document.business.should_not be_blank }
    specify { @document.identifier.should_not be_blank}
  end

  context "should return products (DOM Objects) subject to given -identifier-" do
    specify { @document.products.should_not be_blank }
    specify { @document.products.first.should be_an_instance_of( Product )}
  end
end