require "./spec/spec_helper"
require "./document"

describe Product do
  #  * pick the name of a file and instantiate a document
  before do
    identifier = {
      :product       => '.producttile',
      :id            => 'id',
      :item_name     => 'span.displayname',
      :brand_name    => 'span.designername',
      :image         => '.productimage a img',
      :link          => '.productimage a',
      :regular_price => '.standardprice',
      :sale_price    => '.salesprice'
    }
    @args     = { :business => '<BusinessName>', :url => 'http://www.<business_name>.com/<path_to_page>.html', :identifier => identifier }
    @name     = '<some_name>.html'
    @document = Document.new( @name, @args)
    @product  = @document.products.first
  end

  #  * some methods on product class
  context "have methods to hold and parse XML node" do
    [ :node, :url, :uri, :identifier, :item_name, :brand_name, :link, :image, :regular_price, :sale_price].each do |attribute|
      specify { @product.should respond_to( attribute)}
      specify { @product.send( attribute).should_not be_blank }
    end
    [ :sale_price_value, :regular_price_value, :swap_price_if_required].each do |action|
      specify { @product.send( action).should_not be_blank }
    end
  end

  context "should keep URI parsed from URL" do
    specify { @product.uri.should be_an_instance_of( URI::HTTP )}
  end

  context "prices are always in check" do
    specify { @product.send( :sale_price_value).should_not be_blank }
    specify { @product.send( :regular_price_value).should_not be_blank }
    specify { @product.should be_sale_price_lower }
  end
  
  context "product can intelligently swap sale price lower" do
    before do
      @product.sale_price    = "$000.00" # force sale price higher
      @product.regular_price = "$000.00"
    end
    
    specify do
      @product.send( :swap_price_if_required)
      @product.should be_sale_price_lower
    end
  end

  context "verify the values for this document" do
    before do
      @values = {
        :item_name     => "Grained Low Top Sneaker",
        :brand_name    => "Pierre Hardy",
        :image         => "http://<another_domain>/<path_to_image>/<image_name>.jpg",
        :link          => "http://www.<another_domain>.com/<path_to_another_page>",
        :regular_price => "$000.00",
        :sale_price    => "$000.00"
      }
    end

    #  all verfiiable values are good for respective attributes
    [ :item_name, :brand_name, :link, :image, :regular_price, :sale_price].each do |attribute|
      specify { @product.send( attribute).should == @values[ attribute] }
    end
  end
end