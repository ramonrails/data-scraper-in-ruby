require 'active_support/core_ext/object/blank'
require 'nokogiri'

class Document
  attr_accessor :file_name, :data, :dom_object, :business, :url, :identifier

  def initialize( arg, options = {})
    #  * read and parse the document
    #  * data       = raw data from the file
    #  * dom_object = parsed HTML as object
    parse( arg)
    #  * remember optional parameters
    [ :business, :url, :identifier].each do |_option|
      if (options.class.name == 'Hash') && options.has_key?( _option)
        self.send( "#{_option}=", options[ _option])
      end
    end
  end

  #  search all sub-directories too. any level of depth. just find the file
  def find_file( name)
    unless name.blank?
      self.file_name = Dir.glob( name).first
    end
  end

  #  * does the referenced file exist?
  def exists?
    !self.file_name.blank? && File.exists?( file_name)
  end

  #  * return products as array of Nokogiri DOM objects
  def products
    unless dom_object.blank?
      dom_object.search( identifier[ :product]).collect do |e|
        Product.new( e, { :url => url, :identifier => identifier} )
      end
    end
  end

  # ===================
  # = private methods =
  # ===================
  private

  def read( name)
    find_file( name) if self.file_name.blank? # ensure file_name always holds value
    self.data = File.open( self.file_name, 'r').read rescue nil # do not allow expections to happen
  end

  def parse( arg)
    if ( html = read( arg))
      if (html =~ /html/i) && (html =~ /doctype/i)
        self.dom_object = Nokogiri::HTML( html)
      end
    end
  end
end
